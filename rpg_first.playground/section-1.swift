import Darwin

enum Direction {
    case Up
    case Left
    case Down
    case Right
    func addToPos() -> (Int, Int) {
        switch self {
        case .Up :
            return (-1, 0)
        case .Left:
            return (0, -1)
        case .Down:
            return (1, 0)
        case .Right:
            return (0, 1)
        }
    }
}

func outOfArea(row: Int, column: Int, maxHeight: Int, maxWidth: Int) -> Bool {
    return row <= 0 || row > maxHeight || column <= 0 || column > maxWidth
}

class cell {
    let row: Int
    let column: Int
    let isPassable: Bool
    init(row: Int, column: Int, isPassable: Bool) {
        self.row = row
        self.column = column
        self.isPassable = isPassable
    }
    func onStep(maxHeight: Int, maxWidth: Int) -> String {
        if (outOfArea(row, column, maxHeight, maxWidth)) {
            return "out"
        }
        if (isPassable) {
            return "ok"
        } else {
            return "bad"
        }
    }
}

class grass: cell {
    init(row: Int, column: Int) {
        super.init(row: row, column: column, isPassable: true)
    }
    override func onStep(maxHeight: Int, maxWidth: Int) -> String {
        if (outOfArea(row, column, maxWidth, maxHeight)) {
            return "out"
        }
        return "grass"
    }
}

class mountain: cell {
    init(row: Int, column: Int) {
        super.init(row: row, column: column, isPassable: false)
    }
    override func onStep(maxHeight: Int, maxWidth: Int) -> String {
        if (outOfArea(row, column, maxHeight, maxWidth)) {
            return "out"
        }
        return "mountain"
    }
}

class water: cell {
    init(row: Int, column: Int) {
        super.init(row: row, column: column, isPassable: false)
    }
    override func onStep(maxHeight: Int, maxWidth: Int) -> String {
        if (outOfArea(row, column, maxHeight, maxWidth)) {
            return "out"
        }
        return "water"
    }
}

class map {
    var playersRow: Int
    var playersColumn: Int
    var Height: Int
    var Width: Int
    var array = Array<Array<cell>>()
    init(playersRow: Int, playersColumn: Int, Height: Int, Width: Int) {
        self.playersRow = playersRow
        self.playersColumn = playersColumn
        self.Height = Height
        self.Width = Width
        for row in 0...Height + 1 {
            array.append(Array(count:Width + 2, repeatedValue:cell(row: 0, column: 0, isPassable: false)))
            for column in 0...Width + 1 {
                let chooseCell = Int(arc4random_uniform(5))
                switch chooseCell {
                case 0, 1, 2:
                    array[row][column] = grass(row: row, column: column)
                case 3:
                    array[row][column] = mountain(row: row, column: column)
                case 4:
                    array[row][column] = water(row: row, column: column)
                default:
                    array[row][column] = grass(row: row, column: column)
                }
            }
        }
    }
    func movePlayer(direction: Direction) -> String {
        var (addToRow, addToColumn) = direction.addToPos()
        let nextRow = playersRow + addToRow
        let nextColumn = playersColumn + addToColumn
        var result: String
        result = array[nextRow][nextColumn].onStep(Height, maxWidth: Width)
        switch result {
        case "grass":
            playersRow = nextRow
            playersColumn = nextColumn
            return "player moved to (\(playersRow), \(playersColumn))"
        case "mountain":
            return "sorry, there is a big mountain"
        case "water":
            return "sorry, there is a deep swamp"
        case "out":
            return "sorry, you cannot escape the map"
        default:
            return "wtf"
        }
    }
}

var first = map(playersRow: 6, playersColumn: 6, Height: 6, Width: 6)
for step in 0...30 {
    let chooseDirection = Int(arc4random_uniform(4))
    switch chooseDirection {
    case 0:
        println("up: " + first.movePlayer(Direction.Up))
    case 1:
        println("left: " + first.movePlayer(Direction.Left))
    case 2:
        println("down: " + first.movePlayer(Direction.Down))
    case 3:
        println("right: " + first.movePlayer(Direction.Right))
    default:
        println("wtf")
    }
}